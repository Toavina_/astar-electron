const {app, BrowserWindow, Menu} = require('electron')
const path = require('path')
const url = require('url')
  
  // Gardez l'objet window dans une constante global, sinon la fenêtre sera fermée
  // automatiquement quand l'objet JavaScript sera collecté par le ramasse-miettes.
  let win
  let aboutWin
  
  function createWindow () {
    // Créer le browser window.
    win = new BrowserWindow(
      {
        width: 820,
        height: 600,
        icon: 'src/assets/icon/icon.ico'
      }
    )
  
    // et charge le index.html de l'application.
    win.loadURL(url.format({
      pathname: path.join(__dirname, '/src/index.html'),
      protocol: 'file:',
      slashes: true
    }))


    
  

    win.setResizable(false)
    // Ouvre le DevTools.
    // win.webContents.openDevTools()
  
    // Émit lorsque la fenêtre est fermée.
    win.on('closed', () => {
      // Dé-référence l'objet window , normalement, vous stockeriez les fenêtres
      // dans un tableau si votre application supporte le multi-fenêtre. C'est le moment
      // où vous devez supprimer l'élément correspondant.
      win = null
      app.quit()
    })

    var menu = Menu.buildFromTemplate([
      {
        label: 'Fichier',
        submenu: [
          {
            label: 'Quitter',
            click() {
              
              app.quit()
            }
          }
        ]
      },
      {
        label: 'A propos',
        click() {
          aboutWin = new BrowserWindow({frame:false, width: 400, height: 250})
          aboutWin.setMaximizable(false)
          aboutWin.setResizable(false)
          aboutWin.on('closed', () => aboutWin = null)
          aboutWin.loadURL(url.format({
            pathname: path.join(__dirname, '/src/about.html'),
            protocol: 'file:',
            slashes: true
          }))
          aboutWin.show()
        }
      }
    ])

    Menu.setApplicationMenu(menu)
  }

  
  // Cette méthode sera appelée quant Electron aura fini
  // de s'initialiser et sera prêt à créer des fenêtres de navigation.
  // Certaines APIs peuvent être utilisées uniquement quant cet événement est émit.
  app.on('ready', createWindow)
  
  // Quitte l'application quand toutes les fenêtres sont fermées.
  app.on('window-all-closed', () => {
    // Sur macOS, il est commun pour une application et leur barre de menu
    // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
  
  app.on('activate', () => {
    // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
    // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
    if (win === null) {
      createWindow()
    }
  })
  
  // Dans ce fichier, vous pouvez inclure le reste de votre code spécifique au processus principal. Vous pouvez également le mettre dans des fichiers séparés et les inclure ici.