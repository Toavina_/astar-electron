// ----------------- DEBUT declaration de variable ----------------- //
// taille du grille d'affichage
var col = 40
var rows = 40
var grid = new Array(col)

// liste ouvert
var openSet = []
// liste Fermer
var closeSet = []

// noeud du debut
var start
// noeud fin
var end

// taille d'un noeud
var w,h

// autoriser movement diagonal
var do_diagonal = true

// Chemin optimal
var path = []

// pourcentage d'obstacle
var wallRate = 0.4

// ----------------- FIN declaration de variable ----------------- //


function removeFromArray(arr, el) {
	for(var i=arr.length;i>=0; i--) {
		if(arr[i] === el) {
			arr.splice(i, 1)
		}
	}
}

function heuristic(a,b) {

	// dist p5 function trouve le distance entre deux point
	var d = dist(a.i, a.j, b.i, b.j)
	// var d = abs(a.i-b.i) + abs(a.j-b.j)
	return d
}

// representation noeud
function Spot(i,j) {
	this.i = i
	this.j = j
	// f(x) = g(x) + h(x)
	this.f = 0
	// g(x) cout
	this.g = 0
	// h(x) heuristique du cell
	this.h = 0
	this.neighbors = []
	this.prev = undefined
	this.wall = false;

	// pourcentage mur
	if(random(1) < wallRate) {
		this.wall = true
	}

	this.show = function (cellColor) {
		if (this.wall) {
			//rgb(44, 62, 80)
			fill(color(44, 62, 80));
			noStroke();
			ellipse(this.i * w + w / 2, this.j * h + h / 2, w / 2, h / 2);
		} else if (cellColor){
			fill(cellColor);
			rect(this.i * w, this.j * h, w, h);
		}
		
	}

	this.addNeighbors = function(grid) {
		let i = this.i;
		let j = this.j;
		
		// gauche
		if(i < (col - 1)) {
			this.neighbors.push(grid[ i + 1 ][ j     ]);
		}
		
		// droite
		if(i > 0) {
			this.neighbors.push(grid[ i - 1 ][ j     ]);
		}
		
		// au dessus
		if(j < (rows - 1)) {
			this.neighbors.push(grid[ i     ][ j + 1 ]);
		}
		
		// en dessous
		if(j > 0) {
			this.neighbors.push(grid[ i     ][ j - 1 ]);
		}
		
		if(do_diagonal) {
			// diagonal haut gauche
			if(i > 0 && j > 0) {
				this.neighbors.push(grid[ i - 1 ][ j - 1 ]);
			}
			
			// diagonal haut droite
			if(i < col - 1 && j > 0) {
				this.neighbors.push(grid[ i + 1 ][ j - 1 ]);
			}
			
			// diagonal en desous gauche
			if(i > 0 && j < rows - 1) {
				this.neighbors.push(grid[ i - 1 ][ j + 1 ]);
			}
			
			// diagonal en desous droite
			if(i < col - 1 && j < col - 1) {
				this.neighbors.push(grid[ i + 1 ][ j + 1 ]);
			}
		}
	};

}

// generer une grille
function generateGrid() {
	for(var i=0;i < col; i++) {
		grid[i] = new Array(rows)
	}

	//mise en place d'un tableau 2D
	for(var i=0;i < col; i++) {
		for(var j=0;j < rows; j++) {
			grid[i][j] = new Spot(i,j)	
		}	
	}

	//mise en place voisin
	for(var i=0;i < col; i++) {
		for(var j=0;j < rows; j++) {			
			grid[i][j].addNeighbors(grid)
		}	
	}

}


function generateBoard() {
	// remise a vide des variable
	openSet = [];
	closeSet = [];
	path = [];

	
	
	
	// point de depar et point d'arriver
	start = grid[0][0]
	end = grid[col - 1][rows -1]

	start.wall = false
	end.wall = false

	end.show(color(255,255,0))

	openSet.push(start)
	
	// loop();
}

// evenement recharge d'une map
document.getElementById('start').addEventListener('click', function(){
	wallRate = document.getElementById('wall-rate').value
	document.getElementById('status').innerHTML = ''
	document.getElementById('status').innerHTML = 'Recherche du chemin ...'
	generateGrid()
	generateBoard()
	loop()
	
})

/*
* setup() Fonction obligatoire de P5.js
* insertion du canvas dans le DOM
*/
function setup() {
	var canvas = createCanvas(400, 400);
	
	/*
	*w = canvas largeur
	*h = canvas longeur
	*/	
	w = width / col
	h = height / rows

	canvas.parent('sketch')
	generateGrid()

	
}


/*
*  draw() Fonction obligatoire de P5.js
* draw() un fonction qui est une boucle
* dessine sur canvas
*/
function draw() {


	// --------------------- DEBUT algo Astarr -------------------- //
	if(openSet.length > 0) {
		
		var winner = 0
		// recherche dans openSet le plus petits score de f
		for(var i=0;i < openSet.length; i++) {
			if(openSet[i].f < openSet[winner].f) {
				winner = i
			}
		}
		var current = openSet[winner]
		// si le Sopt courant = Spot end terminer
		if(current === end) {

			noLoop()
			document.getElementById('status').innerHTML = ''
			document.getElementById('status').innerHTML = 'Chemin trouver'
			console.log('Terminer')
		}

		// openSet.remove(current)
		removeFromArray(openSet,current)
		closeSet.push(current)

		var neighbors = current.neighbors

		for(let i = 0; i < neighbors.length; i++) {
			let neighbor = neighbors[ i ];
			
			if(!closeSet.includes(neighbor) && !neighbor.wall) {
				let tempG = current.g + 1;
				
				let newPath = false;
				
				if(openSet.includes(neighbor)) {
					if(tempG < neighbor.g) {
						neighbor.g = tempG;
						newPath = true;
					}
				} else {
					neighbor.g = tempG;
					newPath = true;
					openSet.push(neighbor);
				}
				
				if(newPath) {
					neighbor.h = heuristic(neighbor, end);
					neighbor.f = (neighbor.g + neighbor.h);
					neighbor.prev = current;
				}
			}
		}
	}
	else{
		//pas de solution
		document.getElementById('status').innerHTML = ''
		document.getElementById('status').innerHTML = 'Pas de solution'
		console.log('Pas de solution')
		noLoop()
		return;
		
	}

	// --------------------- FIN algo Astarr -------------------- //
	// --------------------- DEBUT Affichage du resultat -------------------- //
	background(255)

	for(var i=0;i < col; i++) {
		for(var j=0;j < rows; j++) {
			grid[i][j].show(color(255))
		}
		
	}
	
	for(var i=0;i < closeSet.length; i++) {
		closeSet[i].show(color(155, 89, 182,50))
	}

	for (var i = 0; i < openSet.length; i++) {
		openSet[i].show(color(26, 188, 156,50));
	}


	// relier les chemins si il y a une solution rgb(26, 188, 156)

	path = []
	var temp = current
	path.push(current)
	while(temp.prev) {
		path.push(temp.prev)
		temp = temp.prev
	}

	// dessiner ligne chemin rgb(155, 89, 182)
	noFill()
	stroke(155, 89, 182)
	strokeWeight(w/2)
	beginShape()
	for(var i=0;i < path.length;i++) {
		vertex(path[i].i*w+w/2,path[i].j*h+h/2)
	}
	endShape()
}