## Algorithme A*

L'algorithme de recherche A* est un algorithme de recherche de chemin dans un graphe entre un nœud initial et un nœud final tous deux donnés.

L'algorithme A* est illustré par ce programme avec interface graphique programmer en Javascript en utilisant le framework electron.

## script
Installer les dépendances
``` bash
npm install
```
Démarer le programme en developpement

``` bash
npm start
```
Build le programme pour Windows

``` bash
npm run win
```